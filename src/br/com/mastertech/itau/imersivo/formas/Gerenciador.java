package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Gerenciador {

	public void exibirTamanho(Forma pForma) {
		System.out.println(pForma.calcularArea());
	}

	//Forma objForma = new Forma();
	List<Double> ListaLados = new ArrayList<>();
	
	public void IniciarQuestionario() {
		
		Scanner scanner = new Scanner(System.in);
		boolean deveAdicionarNovoLado = true;


		gerarCabecalhoMenu();
		
		while (deveAdicionarNovoLado) {
			System.out.println("Informe o tamanho do lado " + (ListaLados.size() + 1));

			double tamanhoLado = Double.parseDouble(scanner.nextLine());

			if (tamanhoLado <= 0) {
				deveAdicionarNovoLado = false;
			} else {
				ListaLados.add(tamanhoLado);
			}
		}

		System.out.println("Lados cadastrados!");
		System.out.println("Agora vamos calcular a área...");
		
		IdentificarForma();

	}

	private void gerarCabecalhoMenu() {
		System.out.println("Olá! bem vindo ao calculador de área 3 mil!");
		System.out.println("Basta informar a medida de cada lado que eu te digo a área :)");
		System.out.println("Vamos começar!");
		System.out.println("");
		System.out.println("Obs: digite -1 se quiser parar de cadastrar lados!");
		System.out.println("");
	}

	private void IdentificarForma() {

		switch (ListaLados.size()) {
		case (0): {
			System.out.println("Forma inválida");
			break;
		}
		case (1): {
			Circulo circulo = new Circulo();
			circulo.setLados(ListaLados);
			System.out.println(circulo);
			break;
		}
		case (2): {
			Retangulo objRetangulo = new Retangulo();
			objRetangulo.setLados(ListaLados);
			System.out.println(objRetangulo);
			break;
		}
		case (3): {
			Triangulo objTriangulo = new Triangulo();
			objTriangulo.setLados(ListaLados);
			System.out.println(objTriangulo);
			break;
		}
		default: {
			System.out.println("Forma não identificada.");
		}
		}
	}
}
