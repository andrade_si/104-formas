package br.com.mastertech.itau.imersivo.formas;

public class Triangulo extends Forma {

	private boolean TrianguloValido = false;

	@Override
	protected double calcularArea() {
		double ladoA = lados.get(0);
		double ladoB = lados.get(1);
		double ladoC = lados.get(2);
		double area = 0;

		if ((ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA) {
			double s = (ladoA + ladoB + ladoC) / 2;
			area = Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC));
			TrianguloValido = true;
		} else {
			TrianguloValido = false;
		}
		return area;
	}
	
	protected String getNomeForma()
	{
		return "Triângulo";
	}

	public String toString() {
		if (TrianguloValido) {
			return "Eu identifiquei um triangulo!\n" + "A área do triangulo é " + calcularArea();
		} else {
			return "Este triântulo não é válido";
		}
	}
}
