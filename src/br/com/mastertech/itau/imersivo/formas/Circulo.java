package br.com.mastertech.itau.imersivo.formas;

public class Circulo extends Forma {

	@Override
	protected double calcularArea() {
		return Math.PI * Math.pow(lados.get(0), 2);
	}
	
	protected String getNomeForma()
	{
		return "Círculo";
	}
}
