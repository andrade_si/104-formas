package br.com.mastertech.itau.imersivo.formas;

public class Retangulo extends Forma {

	@Override
	protected double calcularArea()
	{
		lados = getLados();
		return (lados.get(0) * lados.get(0));
	}
	
	protected String getNomeForma()
	{
		return "Retângulo";
	}
}
