package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;
import java.util.List;

public abstract class Forma {
	
	protected List<Double> lados;
	protected double area;
	
	
	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
	}

	protected List<Double> getLados()
	{
		return lados;
	}
	
	public void setLados(List<Double> lados) {
		this.lados = lados;
	}

	
	protected abstract double calcularArea();
	protected abstract String getNomeForma();
	
	public String toString()
	{
		return "A área do " + getNomeForma() + " é: " + calcularArea();
	}

}
